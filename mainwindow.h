#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>
#include <QString>
#include <QTextEdit>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void pbChangeDirPressed();

private:

    QString currentDir;

    // Current Directory related
    QLineEdit   *leCurrentDir;
    QPushButton *pbChangeDir;
    QPushButton *pbRescanDir;

    // File List related
    QTextEdit *textEditFiles;


    // Functions
    void prepareGui (void);
    void initVariables(void);
    bool changeDir(const QString &currentDir, QString newDir);
    bool isDirCorrect(const QString dir);
    void parseCurrentDir(void);
};

#endif // MAINWINDOW_H
