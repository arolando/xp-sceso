#include "mainwindow.h"
#include <QDirIterator>
#include <QFileDialog>
#include <QGridLayout>
#include <QGroupBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    prepareGui();
}

MainWindow::~MainWindow()
{

}

void MainWindow::pbChangeDirPressed()
{
    QString newDir;

    if (changeDir(currentDir, newDir)) {
        currentDir = newDir;
    }
}

void MainWindow::prepareGui()
{
    QGridLayout *gridLayout = new QGridLayout();

    // Current Directory related
    pbChangeDir = new QPushButton(tr("Change"));
    connect (pbChangeDir, SIGNAL(clicked()), this, SLOT(pbChangeDirPressed()));

    leCurrentDir = new QLineEdit();
    leCurrentDir->setReadOnly(true);

    gridLayout->addWidget(leCurrentDir, 1,1,Qt::AlignVCenter);
    gridLayout->addWidget(pbChangeDir, 1,7,Qt::AlignVCenter);

    QGroupBox *groupBoxCurrentDir = new QGroupBox(tr("Current directory"));
    groupBoxCurrentDir->setLayout(gridLayout);


    // File List related
    gridLayout = new QGridLayout();

    textEditFiles = new QTextEdit();
    gridLayout->addWidget(textEditFiles, 0, 0, 25,12);

    QGroupBox *groupBoxFileList = new QGroupBox(tr("File List"));
    groupBoxFileList->setLayout(gridLayout);


    // Main window related
    QGridLayout *mainWindowGridLayout = new QGridLayout();

    mainWindowGridLayout->addWidget(groupBoxCurrentDir,0,0);
    mainWindowGridLayout->addWidget(groupBoxFileList,1,0);

    QWidget *w = new QWidget();
    w->setLayout(mainWindowGridLayout);
    setCentralWidget(w);
}

void MainWindow::initVariables()
{

}

bool MainWindow::changeDir(const QString &currentDir, QString newDir)
{
    bool retval = false;

    QFileDialog dirDialog(this);

    dirDialog.setFileMode(QFileDialog::Directory);
    dirDialog.setDirectory(currentDir);

    bool keepGoing = false;
    do {
        if (dirDialog.exec()) {
            QString selectedDir = dirDialog.selectedFiles().first();
            if (isDirCorrect(selectedDir)) {
                newDir = selectedDir;
                retval = true;
                keepGoing = false;
            }

        }
        else {
            keepGoing = false;
        }

    } while (keepGoing);

    return retval;
}

bool MainWindow::isDirCorrect(const QString dir)
{
    bool retval = false;
    QDirIterator iterator(dir);

    while (iterator.hasNext()) {
        QFileInfo fileInfo;
        fileInfo.setFile(iterator.next());
        qDebug() << fileInfo.fileName();

    }
   return retval;
}
